/** 
 *  The project is written to power/control shower cabin periphery such as fan or light.
 *  Target chip is ATTiny13A.
 *  
 *  Tools to build/upload:
 *    - "MicroCore" IDE plugin https://github.com/MCUdude/MicroCore
 *    - Board "ATTiny13"
 *    - Programmer "USBasp"
 *    - Upload thought "Sketch"->"Upload Using Porgrammer"
 */

#include <avr/sleep.h>
#include <avr/sfr_defs.h>
#include <avr/interrupt.h>
#include <util/delay.h>

const uint8_t pinButton = 1;  // 1 is the only pin avaialble for interupt on ATTiny13a
const uint8_t pinLight = 0;
const uint8_t pinFan = 2;
volatile uint8_t lastButtonState = 2; // Undefined
volatile uint8_t periferyState = LOW;

void setup() {
  pinMode(pinButton, INPUT);
  pinMode(pinLight, OUTPUT);
  pinMode(pinFan, OUTPUT);
  
  digitalWrite(pinLight, periferyState);
  digitalWrite(pinFan, periferyState);
  
  attachInterrupt(digitalPinToInterrupt(pinButton), onButtonChanged, CHANGE);  
  
//  set_sleep_mode (SLEEP_MODE_PWR_DOWN);  
//  sleep_enable();
//  sleep_cpu ();  
}

//TODO disable loop and/or use sleep state with interupt wake ups
void loop() {
  // Do nothing
}

void onButtonChanged() {
  int buttonState = digitalRead(pinButton);
  if (buttonState == lastButtonState) return;
  lastButtonState = buttonState;
    
  if (buttonState == HIGH) {
    periferyState = !periferyState;
    digitalWrite(pinLight, periferyState);
    digitalWrite(pinFan, periferyState);
//    sleep_cpu();
  }
}
